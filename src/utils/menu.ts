import { http } from './http'

export const MenuList = (data: string) => {
  return http({
    url: '/Bew/MenuList',
    method: 'POST',
    data,
  })
}
